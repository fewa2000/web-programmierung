from django.apps import AppConfig


class EinkaufslisteConfig(AppConfig):
    name = 'einkaufsliste'
