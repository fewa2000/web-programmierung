from django.apps import AppConfig


class TextadventureConfig(AppConfig):
    name = 'textadventure'
