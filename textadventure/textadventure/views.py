from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.

from . import models

print("Los geht es hier: http://localhost:8000/textadventure/start")

def station(request, name="start"):
    '''
    Hier wird die passende Station geholt und ausgegeben.
    Wird kein Name übergeben, dann wird die Station 'start' geholt.
    '''
    if "choice" in request.POST:
        choice = models.Choice.objects.filter(pk=request.POST["choice"])[0]
        if choice.take_item:
            choice.take_item.take()
        return redirect("station", choice.next_station)

    station = get_object_or_404(models.Station, name=name)
    filtered_choices = []
    for choice in station.choice_set.all():
        if not choice.item_set.filter(owned=False):
            filtered_choices.append(choice)
    return render(request, "textadventure/station.html", {"station": station,
                                                          "choices": filtered_choices})
