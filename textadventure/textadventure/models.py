from django.db import models

# Create your models here.


class Station(models.Model):
    '''
    Eine Station im Textadventure. Der Name ist eine sprechende ID,
    muss also eindeutig (unique) sein.
    '''
    name = models.CharField(max_length=50, unique=True)
    title = models.CharField(max_length=200)
    text = models.TextField()

    def __str__(self):
        return self.name


class Choice(models.Model):
    '''
    Eine Auswahloptin. Neben dem Text zur Beschreibung enthält Sie auch den 
    Verweis auf die nächste Station.
    '''
    station = models.ForeignKey(to=Station, on_delete=models.CASCADE)
    text = models.CharField(max_length=200)
    next_station = models.ForeignKey(to=Station, on_delete=models.CASCADE, related_name="incoming_set")
    take_item = models.ForeignKey(to="Item", on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return f"{self.text}: station:{self.next_station.name} item:{self.take_item}"


class Item(models.Model):
    '''
    Ein Gegenstand, welcher eingesammelt werden kann (owned) und als
    Bedingung für eine Choice dienen kann (condition_for).
    '''
    text = models.CharField(max_length=200)
    owned = models.BooleanField()
    condition_for = models.ForeignKey(to=Choice, on_delete=models.SET_NULL, null=True, blank=True)

    def take(self):
        self.owned = True
        self.save()

    def __str__(self):
        return f"{self.text} -- ({self.condition_for.text})"
