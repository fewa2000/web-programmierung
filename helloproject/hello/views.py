from django.shortcuts import render

# Create your views here.

def hello(request):
    name = "Kai"
    context = {
        "name": name,
    }
    return render(request, "hello/hello.html", context)